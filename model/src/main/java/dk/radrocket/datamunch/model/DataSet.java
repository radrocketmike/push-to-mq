package dk.radrocket.datamunch.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@XmlRootElement(name = "dataset")
@Entity
public class DataSet implements Serializable {
    @Id
    private Integer id;
    private String recipient;
    private String sender;
    private String message;

}
