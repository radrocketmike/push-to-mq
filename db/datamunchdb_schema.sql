CREATE DATABASE IF NOT EXISTS datamunchdb;
USE datamunchdb;
CREATE TABLE dataset(
  id int NOT NULL,
  recipient VARCHAR(100),
  sender VARCHAR(100),
  message VARCHAR(256),
  PRIMARY KEY(id)
);
INSERT INTO dataset
(`id`,
 `recipient`,
 `sender`,
 `message`)
VALUES
  (12,
   'ramsez',
   'nacho',
   'takut eezi');
