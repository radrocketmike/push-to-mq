package dk.radrocket.datamunch.config;

import dk.radrocket.datamunch.mq.MQPublisher;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {

    @Value("${spring.rabbitmq.queuename}")
    private String queueName;

    @Value("${spring.rabbitmq.exchangename}")
    private String exchangeName;

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    public TopicExchange datasetExchange() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    public MQPublisher mqPublisher(RabbitTemplate rabbitTemplate, TopicExchange eventExchange) {
        return new MQPublisher(rabbitTemplate, eventExchange);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(queueName);
    }
}
