package dk.radrocket.datamunch.api;

import dk.radrocket.datamunch.api.spec.PushdataController;
import dk.radrocket.datamunch.model.DataSet;
import dk.radrocket.datamunch.mq.MQPublisher;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * A light controller for pushing data updates in XML format. Returns an empty response with status 200,OK.
 */
@RestController
public class PushdataControllerImpl implements PushdataController {

    @Autowired
    MQPublisher mq;

    private Logger logger = LoggerFactory.getLogger(PushdataController.class);

    @Override
    @RequestMapping(value = "data", method = RequestMethod.POST, consumes = {"application/xml", "application/json"})
    @ResponseStatus(value = HttpStatus.OK)
    public void pushData(@ApiParam(value = "update Data object", required = true) @RequestBody DataSet dataSet) {
        mq.sendMessage(dataSet);
    }
}
