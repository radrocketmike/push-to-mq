package dk.radrocket.datamunch.mq;

import dk.radrocket.datamunch.model.DataSet;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class MQPublisher {

    @Value("${spring.rabbitmq.queuename}")
    private String queueName;

    private final RabbitTemplate rabbitTemplate;
    private final TopicExchange eventExchange;
    private static int msgNumber;

    @Autowired
    public MQPublisher(RabbitTemplate rabbitTemplate, TopicExchange eventExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.eventExchange = eventExchange;
    }

    public void sendMessage(DataSet dataset) {
        rabbitTemplate.convertAndSend(queueName, dataset);
    }
}
