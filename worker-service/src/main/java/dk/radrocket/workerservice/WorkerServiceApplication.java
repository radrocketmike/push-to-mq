package dk.radrocket.workerservice;

import dk.radrocket.datamunch.model.DataSet;
import dk.radrocket.workerservice.config.DataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkerServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(WorkerServiceApplication.class, args);

    }

}

