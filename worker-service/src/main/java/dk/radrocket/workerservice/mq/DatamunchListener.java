package dk.radrocket.workerservice.mq;

import dk.radrocket.datamunch.model.DataSet;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener(queues = "dataset.entry")
public class DatamunchListener {


    @RabbitHandler
    public void receive(DataSet dataSet) {
        System.out.println(" [x] Received '" + dataSet + "'");
    }



}
