package dk.radrocket.workerservice.config;

import dk.radrocket.workerservice.mq.DatamunchListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class RabbitConfig {

    @Bean
    public DatamunchListener rabbitListener(){
        return new DatamunchListener();
    }


}
