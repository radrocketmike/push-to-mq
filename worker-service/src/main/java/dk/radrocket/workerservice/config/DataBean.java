package dk.radrocket.workerservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class DataBean {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public DataBean(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        int total = this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM dataset", Integer.class);
        System.out.println("********: " + total);
    }
}
