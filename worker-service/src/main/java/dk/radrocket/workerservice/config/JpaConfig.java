package dk.radrocket.workerservice.config;

import dk.radrocket.datamunch.model.DataSet;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan(basePackageClasses=DataSet.class)
public class JpaConfig {
}
